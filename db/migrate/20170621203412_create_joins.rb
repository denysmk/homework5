class CreateJoins < ActiveRecord::Migration[5.1]
  def change
    create_table :joins do |t|
      t.boolean :go, default: false
      t.integer :user_id
      t.integer :event_id
      t.timestamps
    end
  end
end
