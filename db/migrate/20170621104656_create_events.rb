class CreateEvents < ActiveRecord::Migration[5.1]
  def change
    create_table :events do |t|
      t.string :title
      t.text :content
      t.datetime :date_of_event

      t.timestamps
    end
  end
end
