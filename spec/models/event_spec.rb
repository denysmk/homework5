require 'rails_helper'

describe Event do
  before(:all) do
    @admin = User.create(id:1, email: "admin@a.com", admin: true)
    @user = User.create(id:2, email: "user@a.com")
    @event = Event.create(id:1, title: "pivorak", content: "5th lection about Rails")
  end

  it 'shoud be created' do
    expect(@event.title).to include("pivorak")
  end

  it 'shoud be deleted' do
    @event.destroy
    expect(Event.all.count).to eq(0)
  end

  it 'comment can be created' do
    @comment = Comment.new(content: 'new', user_id: 1, event_id: 1)
    expect(@comment.content).to include("new")
  end

end