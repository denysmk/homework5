json.extract! event, :id, :title, :content, :date_of_event, :created_at, :updated_at
json.url event_url(event, format: :json)
